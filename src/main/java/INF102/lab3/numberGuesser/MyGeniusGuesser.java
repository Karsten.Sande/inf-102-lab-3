package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int last = number.getUpperbound();
        int first = number.getLowerbound();
        int mid = (first + last)/2;

        while (first <= last){
            int newGuess = number.guess(mid);
            if (newGuess == 0){
                return mid;
            }
            if (newGuess == -1){
                first = mid + 1;
            }
            else {
                last = mid -1;
            }
            mid = (first + last)/2;
        }
        return 0;
    }
}
