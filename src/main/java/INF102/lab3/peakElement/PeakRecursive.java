package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        int n = numbers.size();

        if (n == 1){
            return numbers.get(0);
        }
        if (numbers.get(0) > numbers.get(1)){
            return numbers.get(0);
        }
        if (numbers.get(n-1) > numbers.get(n-2)){
            return numbers.get(n-1);
        }

        return peakFinder(numbers);
    }
    public int peakFinder(List<Integer> numbers){
        int midpoint = (numbers.size())/2;
        int mid = numbers.get(midpoint);
        int left = numbers.get(midpoint-1);

        if (left > mid){
            return peakElement(numbers.subList(0, midpoint));
        }
        else{
            return peakElement(numbers.subList(midpoint, numbers.size()-1));
        }
    }
}
