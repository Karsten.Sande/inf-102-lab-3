package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        if (list.size() == 0){
            return 0;
        }
        int index = list.size()-1;
        long value = list.get(index);
        list.remove(index);
        return value + sum(list);
    }

}
    


